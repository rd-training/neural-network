package be.gfi.academy.core;

import be.gfi.academy.functions.activation.ActivationFunction;
import be.gfi.academy.functions.summing.SummingFunction;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NeuralLayer {
	private LayerType layerType;
	private List<Neuron> neurons;
	private boolean hasBias;

	public static long seed = 0;
	public static Random generator = new Random(seed);

	public NeuralLayer(LayerType layerType, SummingFunction summingFunction, ActivationFunction activationFunction, int amount, List<List<Double>> weights, boolean bias){
		this(layerType, null, summingFunction, activationFunction, amount, weights, bias);
	}

	public NeuralLayer(LayerType layerType, NeuralLayer previousNeuralLayer, SummingFunction summingFunction, ActivationFunction activationFunction, int amount, List<List<Double>> weights, boolean bias){
		this.layerType = layerType;
		neurons = new ArrayList<>();
		if(bias){
			hasBias= true;
			amount++;
		}
		for(int i = 0; i < amount; i++){
			Neuron neuron = new Neuron(summingFunction, activationFunction, this);
			if(i == amount-1 && bias){
				neuron.setValue(1.0);
				neuron.setBias();
			} else {
				if(previousNeuralLayer != null){
					int counter = 0;
					for(Neuron previousNeuron : previousNeuralLayer.getNeurons()){
						NeuralLink neuralLink = new NeuralLink(previousNeuron, neuron, (weights == null || weights.size() <= i) ? generator.nextDouble() : weights.get(i).get(counter));
						previousNeuron.addOutput(neuralLink);
						neuron.addInput(neuralLink);
						counter++;
					}
				}
			}
			neurons.add(neuron);
		}
	}

	public List<Neuron> getNeurons(){
		return neurons;
	}

	/*
		TODO: propagate in each neurons
	 */
	public void propagate(){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO: activate each neurons
	 */
	public void activate(){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO, throw a runtime exception if the input list does not match the amount of neurons. Don't forget about that a bias neuron's input is always 1.
	 */
	public void setInput(List<Double> inputs){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO
	 */
	public List<Double> getValues(){
		throw new RuntimeException("Function not implemented.");
	}

	public JsonObject save(){
		JsonObject jsonObject = new JsonObject();
		jsonObject.addProperty("layerType", layerType.toString());
		jsonObject.addProperty("hasBias", hasBias);
		jsonObject.addProperty("neurons", neurons.size());
		JsonArray jsonArray = new JsonArray();
		if(!layerType.equals(LayerType.INPUT_SUM_LINEAR) && !layerType.equals(LayerType.INPUT_SUM_NORM)){
			for(Neuron neuron : neurons){
				JsonArray neuronWeights = new JsonArray();
				for(NeuralLink neuralLink : neuron.getInputs()){
					neuronWeights.add(neuralLink.getWeight());
				}
				jsonArray.add(neuronWeights);
			}
		}
		jsonObject.add("weights", jsonArray);
		return jsonObject;
	}
}
