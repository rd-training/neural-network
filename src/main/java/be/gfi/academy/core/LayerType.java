package be.gfi.academy.core;

public enum LayerType {
	INPUT_SUM_LINEAR, INPUT_SUM_NORM, HIDDEN_SUM_SIGMOID, OUTPUT_SUM_SIGMOID
}
