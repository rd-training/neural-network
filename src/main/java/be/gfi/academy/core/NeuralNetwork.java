package be.gfi.academy.core;

import be.gfi.academy.functions.activation.LinearFunction;
import be.gfi.academy.functions.activation.SigmoidFunction;
import be.gfi.academy.functions.summing.SumFunction;
import be.gfi.academy.util.TrainingData;
import com.google.gson.*;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

public class NeuralNetwork {
	private NeuralLayer input;
	private List<NeuralLayer> hidden;
	private NeuralLayer output;

	public NeuralNetwork(){
		hidden = new ArrayList<>();
	}

	public NeuralNetwork(String filename){
		this();
		load(filename);
	}

	public void addLayer(LayerType layerType, int amount, boolean bias){
		addLayer(layerType, amount, null, bias);
	}

	public void addLayer(LayerType layerType, int amount, List<List<Double>> weights, boolean bias){
		NeuralLayer previousLayer = input;
		if(hidden.size() > 0){
			previousLayer = hidden.get(hidden.size()-1);
		}
		switch(layerType){
		case INPUT_SUM_LINEAR:
				input = new NeuralLayer(layerType, SumFunction.getInstance(), LinearFunction.getInstance(), amount, weights, bias);
				break;
			case HIDDEN_SUM_SIGMOID:
				hidden.add(new NeuralLayer(layerType, previousLayer, SumFunction.getInstance(), SigmoidFunction.getInstance(), amount, weights, bias));
				break;
			case OUTPUT_SUM_SIGMOID:
				output = new NeuralLayer(layerType, previousLayer, SumFunction.getInstance(), SigmoidFunction.getInstance(), amount, weights, bias);
				break;
		}
	}

	/*
		TODO, part of the forward propagation
	 */
	public List<Double> predict(List<Double> inputs){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO: tip: training is a sequence of predictions, back propagation and weights updates given a number of epochs.
	 */
	public void train(List<TrainingData> trainingDataSet, double rate, int epoch){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO
	 */
	private void backPropagate(List<Double> expected){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO, to make it easier, call the private function updateLayerWeight
	 */
	private void updateWeights(double lRate){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO
	 */
	private void updateLayerWeight(double lRate, NeuralLayer neuralLayer){
		throw new RuntimeException("Function not implemented.");
	}

	public void save(String filename){
		JsonArray jsonArray = new JsonArray();
		jsonArray.add(input.save());
		for(NeuralLayer neuralLayer : hidden){
			jsonArray.add(neuralLayer.save());
		}
		jsonArray.add(output.save());
		try (Writer writer = new FileWriter(filename)) {
			Gson gson = new GsonBuilder().create();
			gson.toJson(jsonArray, writer);
		} catch(Exception e){
			e.printStackTrace();
		}
	}

	private void load(String filename){
		JsonParser parser = new JsonParser();
		try {
			JsonElement jsontree = parser.parse(new FileReader(filename));
			JsonArray jsonArray = jsontree.getAsJsonArray();
			for(int i = 0; i < jsonArray.size(); i++){
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				LayerType layerType = LayerType.valueOf(jsonObject.get("layerType").getAsString());
				boolean hasBias = jsonObject.get("hasBias").getAsBoolean();
				int neurons = jsonObject.get("neurons").getAsInt();
				List<List<Double>> weights = new ArrayList<>();
				JsonArray weightsArray = jsonObject.get("weights").getAsJsonArray();
				for(int j = 0; j < weightsArray.size(); j++){
					JsonArray weightsNeuron = weightsArray.get(j).getAsJsonArray();
					List<Double> wn = new ArrayList<>();
					for(int h = 0; h < weightsNeuron.size(); h++){
						wn.add(weightsNeuron.get(h).getAsDouble());
					}
					weights.add(wn);
				}
				this.addLayer(layerType, neurons, weights, hasBias);
			}
		} catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}

	public NeuralLayer getInput() {
		return input;
	}

	public NeuralLayer getOutput() {
		return output;
	}

	public List<NeuralLayer> getHidden() {
		return hidden;
	}
}
