package be.gfi.academy.core;

public class NeuralLink {
	private Neuron from;
	private Neuron to;
	private double weight;

	public NeuralLink(Neuron from, Neuron to, double weight){
		this.from = from;
		this.to = to;
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight){
		this.weight = weight;
	}

	/*
		TODO
	 */
	public double getWeightedValue(){
		throw new RuntimeException("Function not implemented.");
	}

	public Neuron getFrom(){
		return from;
	}

	public Neuron getTo() {
		return to;
	}
}
