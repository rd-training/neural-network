package be.gfi.academy.core;

import be.gfi.academy.functions.activation.ActivationFunction;
import be.gfi.academy.functions.summing.SummingFunction;

import java.util.ArrayList;
import java.util.List;

public class Neuron {
	private List<NeuralLink> inputs;
	private List<NeuralLink> outputs;

	private NeuralLayer layer;

	private SummingFunction summingFunction;
	private ActivationFunction activationFunction;

	private double value;

	private double error;
	private double delta;

	private boolean bias;

	public Neuron(SummingFunction summingFunction, ActivationFunction activationFunction, NeuralLayer neuralLayer){
		this.inputs = new ArrayList<>();
		this.outputs = new ArrayList<>();
		this.summingFunction = summingFunction;
		this.activationFunction = activationFunction;
		this.layer = neuralLayer;
	}

	public void setValue(double value){
		this.value = value;
	}

	public double getValue(){
		return value;
	}

	/*
		TODO, part of the forward propagation
	 */
	public void propagate(){
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO, part of the forward propagation
	 */
	public void activate(){
		throw new RuntimeException("Function not implemented.");
	}

	public List<NeuralLink> getInputs() {
		return inputs;
	}

	public void addInput(NeuralLink input) {
		inputs.add(input);
	}

	public List<NeuralLink> getOutputs() {
		return outputs;
	}

	public void addOutput(NeuralLink output) {
		outputs.add(output);
	}

	public void setError(double error){
		this.error = error;
	}

	public double getError(){
		return error;
	}

	public void setDelta(double delta){
		this.delta = delta;
	}

	public double getDelta(){
		return delta;
	}

	/*
		TODO, part of the back propagation
	 */
	public double getDerivative(){
		throw new RuntimeException("Function not implemented.");
	}

	public void setBias(){
		bias = true;
	}

	public boolean isBias() {
		return bias;
	}
}
