package be.gfi.academy;

import be.gfi.academy.util.Validation;

public class Main {

	public static void main(String[] args){
		//OptimalFinder in util can be used to fin optimal seeds values.
		Validation.validateSimpleBinary();
		Validation.validateSinusZeroToOne();
	}
}
