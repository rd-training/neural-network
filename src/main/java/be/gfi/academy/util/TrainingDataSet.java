package be.gfi.academy.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class TrainingDataSet {
	List<TrainingData> trainingDataList;

	public TrainingDataSet(){
		trainingDataList = new ArrayList<>();
	}

	public TrainingDataSet(String filename){
		this();
		load(filename);
	}

	public List<TrainingData> getTrainingDataList(){
		return trainingDataList;
	}

	private void load(String filename){
		JsonParser parser = new JsonParser();
		try {
			JsonElement jsontree = parser.parse(new FileReader(filename));
			JsonArray jsonArray = jsontree.getAsJsonArray();
			for(int i = 0; i < jsonArray.size(); i++){
				TrainingData trainingData = new TrainingData();
				JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
				JsonArray values = jsonObject.getAsJsonArray("input");
				JsonArray expected = jsonObject.getAsJsonArray("expected");
				for(int j = 0; j < values.size(); j++){
					trainingData.addValue(values.get(j).getAsDouble());
				}

				for(int j = 0; j < expected.size(); j++){
					trainingData.addExpected(expected.get(j).getAsDouble());
				}
				trainingDataList.add(trainingData);
			}
		} catch(Exception e){
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
	}
}
