package be.gfi.academy.util;

import be.gfi.academy.core.LayerType;
import be.gfi.academy.core.NeuralLayer;
import be.gfi.academy.core.NeuralNetwork;
import org.knowm.xchart.QuickChart;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.style.Styler;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Validation {

    public static void validateSinusZeroToOne(){
        double step = 0.05;
        long seed = 6535752521735700311L;
        TrainingDataSet trainingDataSet = new TrainingDataSet("sin_dataset.json");
        NeuralNetwork neuralNetwork = new NeuralNetwork();
        neuralNetwork.addLayer(LayerType.INPUT_SUM_LINEAR, 1, true);
        neuralNetwork.addLayer(LayerType.HIDDEN_SUM_SIGMOID, 10, true);
        neuralNetwork.addLayer(LayerType.HIDDEN_SUM_SIGMOID, 10, true);
        neuralNetwork.addLayer(LayerType.OUTPUT_SUM_SIGMOID, 1, false);
        NeuralLayer.seed = seed;
        NeuralLayer.generator = new Random(seed);
        neuralNetwork.train(trainingDataSet.getTrainingDataList(), step, 5000000);

        List<Double> test = new ArrayList<>();
        test.add(1.0);
        List<Double> outputA = neuralNetwork.predict(test);
        test = new ArrayList<>();
        test.add(0.0);
        List<Double> outputB = neuralNetwork.predict(test);

        test = new ArrayList<>();
        test.add(0.5);
        List<Double> outputC = neuralNetwork.predict(test);
        System.out.println("0.5 : "+outputC.toString());

        test = new ArrayList<>();
        test.add(0.35);
        outputC = neuralNetwork.predict(test);
        System.out.println("0.35 : "+outputC.toString());

        test = new ArrayList<>();
        test.add(0.25);
        outputC = neuralNetwork.predict(test);
        System.out.println("0.25 : "+outputC.toString());

        test = new ArrayList<>();
        test.add(0.75);
        outputC = neuralNetwork.predict(test);
        System.out.println("0.75 : "+outputC.toString());

        test = new ArrayList<>();
        test.add(0.71);
        outputC = neuralNetwork.predict(test);
        System.out.println("0.71 : "+outputC.toString());

        plotSinusFunction(neuralNetwork);
    }

    public static void validateSimpleBinary(){
        double step = 0.05;
        long seed = 586658357448622169L;
        TrainingDataSet trainingDataSet = new TrainingDataSet("test_dataset.json");
        NeuralNetwork neuralNetwork = new NeuralNetwork();
        neuralNetwork.addLayer(LayerType.INPUT_SUM_LINEAR, 4, true);
        neuralNetwork.addLayer(LayerType.HIDDEN_SUM_SIGMOID, 10, true);
        neuralNetwork.addLayer(LayerType.OUTPUT_SUM_SIGMOID, 2, false);
        NeuralLayer.seed = seed;
        NeuralLayer.generator = new Random(seed);
        neuralNetwork.train(trainingDataSet.getTrainingDataList(), step, 2000);

        List<Double> test = new ArrayList<>();
        test.add(1.0);
        test.add(0.0);
        test.add(0.0);
        test.add(0.0);
        List<Double> outputA = neuralNetwork.predict(test);
        System.out.println("input: "+test+" --- output: "+outputA+" --- expected: [1,0]");
        test = new ArrayList<>();
        test.add(0.0);
        test.add(0.0);
        test.add(0.0);
        test.add(0.0);
        List<Double> outputB = neuralNetwork.predict(test);
        System.out.println("input: "+test+" --- output: "+outputB+" --- expected: [0,0]");
        test = new ArrayList<>();
        test.add(0.0);
        test.add(1.0);
        test.add(0.0);
        test.add(0.0);
        List<Double> outputC = neuralNetwork.predict(test);
        System.out.println("input: "+test+" --- output: "+outputC+" --- expected: [0,1]");
    }

    private static void plotSinusFunction(NeuralNetwork neuralNetwork){
        String[] series = new String[] {"original sin(x)", "approximated sin(x)"};
        int max = 1;
        double step = 0.01;
        int amount = (int) (max/step);
        double[] xData = new double[amount];
        int counter = 0;
        for(int i = 0; i < xData.length; i++){
            xData[i] = step*i;
        }
        double[][] yData = new double[2][amount];
        for(double x : xData){
            yData[0][counter] = Math.sin(x);
            ArrayList arrayList = new ArrayList();
            arrayList.add(x);
            yData[1][counter] = (double) neuralNetwork.predict(arrayList).get(0);
            counter++;
        }
        //double[][] yData = new double[][] { {2.0, 1.0, 0.0}, {1.0, 0.0, -1.0} };

        XYChart chart = QuickChart.getChart("Comparison sin(x) and approximation", "X", "Y", series, xData, yData);
        chart.getStyler().setLegendPosition(Styler.LegendPosition.InsideSE);
        chart.getStyler().setLegendVisible(true);
        new SwingWrapper(chart).displayChart();
    }
}
