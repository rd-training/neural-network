package be.gfi.academy.util;

import java.util.ArrayList;
import java.util.List;

public class TrainingData {
	private List<Double> values;
	private List<Double> expected;

	public TrainingData(){
		values = new ArrayList<>();
		expected = new ArrayList<>();
	}

	public List<Double> getValues(){
		return values;
	}

	public void setValues(List<Double> value){
		this.values = value;
	}

	public void addValue(double value){
		values.add(value);
	}

	public List<Double> getExpected() {
		return expected;
	}

	public void addExpected(double expected){
		this.expected.add(expected);
	}

	public void setExpected(List<Double> expected){
		this.expected = expected;
	}
}
