package be.gfi.academy.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CsvParser {


	public static void main(String[] args){
		try (
			Reader reader = Files.newBufferedReader(Paths.get("noordstraat.csv"));
			CSVParser csvParser = new CSVParser(reader, CSVFormat.EXCEL.withDelimiter(';'));
		) {
			JsonArray jsonArray = new JsonArray();
			for (CSVRecord csvRecord : csvParser){
				if(csvRecord.size() < 9){
					continue;
				}
				JsonObject jsonObject = new JsonObject();
				JsonArray values = new JsonArray();
				values.add(csvRecord.get(2));
				values.add(csvRecord.get(3));
				values.add(csvRecord.get(4));
				values.add(csvRecord.get(5));
				JsonArray expected = new JsonArray();
				int category = Integer.valueOf(csvRecord.get(9));
				for(int i = 0; i < 2; i++){
					if(i == category){
						expected.add(1);
					} else {
						expected.add(0);
					}
				}
				jsonObject.add("input", values);
				jsonObject.add("expected", expected);
				jsonArray.add(jsonObject);
				try (Writer writer = new FileWriter("noordstraat_dataset.json")) {
					Gson gson = new GsonBuilder().create();
					gson.toJson(jsonArray, writer);
				} catch(Exception e){
					e.printStackTrace();
				}
			}

		} catch (Exception e){
			e.printStackTrace();
		}
	}
}
