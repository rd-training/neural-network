package be.gfi.academy.util;

import be.gfi.academy.core.LayerType;
import be.gfi.academy.core.NeuralLayer;
import be.gfi.academy.core.NeuralNetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class OptimalFinder {

    /*
        @args:
            maxSeeds: amount of random seed to try
            epsilon: maximum difference between expected and output
            percentageTested: minimum threshold of tested values within epsilon (between 0 and 1)
            dataset: data set file name
     */
    public static void findOptimal(int maxSeeds, double epsilon, double percentageTested, String dataset){
        TrainingDataSet trainingDataSet = new TrainingDataSet(dataset);
        Random random = new Random();
        List<Long> seeds = new ArrayList<>();
        for(long i = 0; i < maxSeeds; i++){
            seeds.add(random.nextLong());
        }
        List<Double> steps = new ArrayList<>(Arrays.asList(0.7,0.5,0.2,0.05));
        for(Double value : steps){
            new Thread(() -> {
                for(long seed : seeds){
                    run(seed, value, epsilon, percentageTested, trainingDataSet);
                }
            }).start();
        }
    }

    private static void run(long seed, double step, double epsilon, double percentageTested, TrainingDataSet trainingDataSet){
        NeuralNetwork neuralNetwork = new NeuralNetwork();
        neuralNetwork.addLayer(LayerType.INPUT_SUM_LINEAR, 4, true);
        neuralNetwork.addLayer(LayerType.HIDDEN_SUM_SIGMOID, 10, true);
        neuralNetwork.addLayer(LayerType.OUTPUT_SUM_SIGMOID, 2, false);
        NeuralLayer.seed = seed;
        NeuralLayer.generator = new Random(seed);
        neuralNetwork.train(trainingDataSet.getTrainingDataList(), step, 2000);

        int valid = 0;

        for(TrainingData trainingData : trainingDataSet.getTrainingDataList()){
            List<Double> output = neuralNetwork.predict(trainingData.getValues());
            List<Double> expected = trainingData.getExpected();
            for(int i = 0; i < output.size(); i++){
                if(Math.abs(output.get(i)-expected.get(i)) < epsilon){
                    valid++;
                }
            }
        }

        if(valid/trainingDataSet.getTrainingDataList().size() > percentageTested){
            System.out.println("potentially valid seed: "+seed+" - steps: "+step);
        }
    }
}
