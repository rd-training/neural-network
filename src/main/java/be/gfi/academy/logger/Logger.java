package be.gfi.academy.logger;

import be.gfi.academy.core.NeuralLayer;
import be.gfi.academy.core.NeuralLink;
import be.gfi.academy.core.NeuralNetwork;
import be.gfi.academy.core.Neuron;

import java.util.List;

public class Logger {

	public static void logNeurons(NeuralNetwork neuralNetwork){
		StringBuilder sb = new StringBuilder();
		NeuralLayer neuralLayer = neuralNetwork.getInput();
		System.out.println("Layer input:\n");
		sb.append("Layer input:\n");
		int counter = 0;
		for(Neuron neuron : neuralLayer.getNeurons()){
			sb.append("\t").append("- neuron ").append(counter).append(":\n").append(log(neuron));
			counter++;
		}
		List<NeuralLayer> neuralLayers = neuralNetwork.getHidden();
		int layerCounter = 0;
		for(NeuralLayer neuralLayerHidden : neuralLayers){
			sb.append("Layer hidden ").append(layerCounter).append(":\n");
			counter = 0;
			for(Neuron neuron : neuralLayerHidden.getNeurons()){
				sb.append("\t").append("- neuron ").append(counter).append(":\n").append(log(neuron));
				counter++;
			}
			layerCounter++;
		}

		neuralLayer = neuralNetwork.getOutput();
		sb.append("Layer output:\n");
		counter = 0;
		for(Neuron neuron : neuralLayer.getNeurons()){
			sb.append("\t").append("- neuron ").append(counter).append(":\n").append(log(neuron));
			counter++;
		}

		System.out.println(sb);
	}

	private static StringBuilder log(Neuron neuron){
		StringBuilder sb = new StringBuilder();
		sb.append("\t\t").append("> inputs ").append(neuron.getInputs().size()).append("\n");
		sb.append("\t\t").append("> ouputs ").append(neuron.getOutputs().size()).append("\n");
		sb.append("\t\t").append("> error ").append(neuron.getError()).append("\n");
		sb.append("\t\t").append("> delta ").append(neuron.getDelta()).append("\n");
		sb.append("\t\t").append("> value ").append(neuron.getValue()).append("\n");
		return sb;
	}

	public static void logLinks(NeuralNetwork neuralNetwork){
		StringBuilder sb = new StringBuilder();
		List<NeuralLayer> neuralLayers = neuralNetwork.getHidden();
		int layerCounter = 0;
		for(NeuralLayer neuralLayerHidden : neuralLayers){
			sb.append("Layer hidden ").append(layerCounter).append(":\n");
			int neuronCounter = 0;
			for(Neuron neuron : neuralLayerHidden.getNeurons()){
				sb.append("\t").append("- neuron ").append(neuronCounter).append(":\n").append(log(neuron));
				int neuralCounter = 0;
				for(NeuralLink neuralLink : neuron.getInputs()){
					sb.append("\t\t").append("neural link ").append(neuralCounter).append(":\n").append(log(neuralLink));
					neuralCounter++;
				}
				neuronCounter++;
			}
			layerCounter++;
		}

		NeuralLayer neuralLayer = neuralNetwork.getOutput();
		sb.append("Layer output:\n");
		int neuronCounter = 0;
		for(Neuron neuron : neuralLayer.getNeurons()){
			sb.append("\t").append("- neuron ").append(neuronCounter).append(":\n").append(log(neuron));
			int neuralCounter = 0;
			for(NeuralLink neuralLink : neuron.getInputs()){
				sb.append("\t\t").append("neural link ").append(neuralCounter).append(":\n").append(log(neuralLink));
				neuralCounter++;
			}
			neuronCounter++;
		}

		System.out.println(sb);
	}

	private static StringBuilder log(NeuralLink neuralLink){
		StringBuilder sb = new StringBuilder();
		sb.append("\t\t\t").append("> weight ").append(neuralLink.getWeight()).append("\n");
		sb.append("\t\t\t").append("> weightedValue ").append(neuralLink.getWeightedValue()).append("\n");
		return sb;
	}

	public static void logLayers(NeuralNetwork neuralNetwork){
		StringBuilder sb = new StringBuilder();
		NeuralLayer neuralLayer = neuralNetwork.getInput();
		sb.append("Layer input: neurons:").append(neuralLayer.getNeurons().size()).append(":\n");
		List<NeuralLayer> neuralLayers = neuralNetwork.getHidden();
		int layerCounter = 0;
		for(NeuralLayer neuralLayerHidden : neuralLayers) {
			sb.append("Layer hidden ").append(layerCounter).append(": neurons:").append(neuralLayerHidden.getNeurons().size()).append(":\n");
			layerCounter++;
		}
		neuralLayer = neuralNetwork.getOutput();
		sb.append("Layer output: neurons:").append(neuralLayer.getNeurons().size()).append(":\n");
		System.out.println(sb);
	}
}
