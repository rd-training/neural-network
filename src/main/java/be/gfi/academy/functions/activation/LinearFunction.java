package be.gfi.academy.functions.activation;

import be.gfi.academy.core.NeuralLayer;
import be.gfi.academy.core.Neuron;

public class LinearFunction implements ActivationFunction {
	private static LinearFunction instance = null;

	private LinearFunction(){

	}

	public static LinearFunction getInstance(){
		if(instance == null){
			instance = new LinearFunction();
		}
		return instance;
	}

	/*
		TODO
	 */
	@Override public double activate(Neuron value, NeuralLayer neuralLayer) {
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO
	 */
	@Override public double getDerivative(Neuron neuron, NeuralLayer neuralLayer) {
		throw new RuntimeException("Function not implemented.");
	}
}
