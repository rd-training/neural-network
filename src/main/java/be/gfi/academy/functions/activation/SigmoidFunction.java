package be.gfi.academy.functions.activation;

import be.gfi.academy.core.NeuralLayer;
import be.gfi.academy.core.Neuron;

public class SigmoidFunction implements ActivationFunction {
	private static SigmoidFunction instance = null;

	private SigmoidFunction(){

	}

	public static SigmoidFunction getInstance(){
		if(instance == null){
			instance = new SigmoidFunction();
		}
		return instance;
	}

	/*
		TODO
	 */
	@Override
	public double activate(Neuron value, NeuralLayer neuralLayer) {
		throw new RuntimeException("Function not implemented.");
	}

	/*
		TODO
	 */
	@Override
	public double getDerivative(Neuron neuron, NeuralLayer neuralLayer) {
		throw new RuntimeException("Function not implemented.");
	}
}
