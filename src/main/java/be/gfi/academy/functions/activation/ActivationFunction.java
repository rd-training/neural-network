package be.gfi.academy.functions.activation;

import be.gfi.academy.core.NeuralLayer;
import be.gfi.academy.core.Neuron;

public interface ActivationFunction {
	public double activate(Neuron value, NeuralLayer neuralLayer);

	public double getDerivative(Neuron neuron, NeuralLayer neuralLayer);
}
