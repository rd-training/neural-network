package be.gfi.academy.functions.summing;

import be.gfi.academy.core.NeuralLink;

import java.util.List;

public class SumFunction implements SummingFunction {
	private static SumFunction instance = null;

	private SumFunction(){

	}

	public static SumFunction getInstance(){
		if(instance == null){
			instance = new SumFunction();
		}
		return instance;
	}

	/*
		TODO
	 */
	@Override
	public double sumInputs(List<NeuralLink> neurons) {
		throw new RuntimeException("Function not implemented.");
	}
}
