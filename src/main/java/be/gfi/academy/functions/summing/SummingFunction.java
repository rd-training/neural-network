package be.gfi.academy.functions.summing;


import be.gfi.academy.core.NeuralLink;

import java.util.List;

public interface SummingFunction {
	public double sumInputs(List<NeuralLink> neurons);
}
